import { html, css, LitElement } from 'lit-element';

export class ItemCard extends LitElement {
  // language=CSS
  static get styles() {
    return css`
      :host {
        --lit-sopra-text-color: #000;

        display: inline-block;
        font-family: arial, serif;
        padding: 25px;
        /*color: var(--lit-sopra-text-color, #666666);*/
        color: #666666;
      }
      .card {
        display: flex;
        flex-direction: column;
      }
      .picture {
        max-width: 200px;
        max-height: 300px;
        border: 1px solid #d3d3d3;
        border-radius: 10px;
        overflow: hidden;
        cursor: pointer;
        transition: all 0.2s ease-in-out;
      }
      .picture:hover {
        transform: scale(1.1);
      }
      img {
        width: 100%;
        object-fit: cover;
        height: 100%;
      }
      .rating {
        display: flex;
        justify-content: space-between;
        align-items: center;
      }
      ul {
        display: flex;
        list-style: none;
        padding: 0;
        margin: 0;
      }
      li {
        margin: 0 5px;
      }
      .star {
        font-size: 20px;
        display: block;
        z-index: 1;
        position: relative;
        line-height: 100%;
      }
      .filled {
        color: gold;
      }
    `;
  }

  static get properties() {
    return {
      value: {
        type: String,
      },
      title: {
        type: String,
        value: 'Peli',
      },
      rating: {
        type: Number,
        value: 0,
      },
    };
  }

  firstUpdated() {
    this.fillRating(this.rating);
  }

  fillRating(rate) {
    const rating = Math.floor(rate);
    const stars = this.shadowRoot.querySelectorAll('.star');

    stars.forEach((item, i) => {
      if (i < rating) {
        item.classList.add('filled');
      } else {
        item.classList.remove('filled');
      }
    });
  }

  render() {
    return html`
      <div class="card">
        <div class="picture">
          <img
            src="https://rockthebestmusic.com/wp-content/uploads/2015/06/The-Godfather-Poster.jpg"
            alt=""
          />
        </div>
        <h3>${this.title}</h3>
        <div class="rating">
          <span>${this.rating}</span>
          <div class="stars">
            <ul>
              <li><span class="star">&#9733;</span></li>
              <li><span class="star">&#9733;</span></li>
              <li><span class="star">&#9733;</span></li>
              <li><span class="star">&#9733;</span></li>
              <li><span class="star">&#9733;</span></li>
            </ul>
          </div>
        </div>
        <rating></rating>
      </div>
    `;
  }
}
